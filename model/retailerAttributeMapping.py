from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, ForeignKeyConstraint, UniqueConstraint
from sqlalchemy.types import Boolean, Integer, String

from cot.lib.util import Log

from atommodel.model import Base, StateEntity
from atommodel.model.mixins.basic_export import BasicExport
from atommodel.model.util import get_model

model = get_model()

log = Log(__name__)

class RetailerAttributeMapping(Base, StateEntity, BasicExport):
    """
===========================
 **CSV export**
===========================
-----------------
 **CSV columns**
-----------------
    * dome_identifier
    * retailer_code
    * business_name
    * retailer_category_raw
    * status
    * retailer_attribute_code
    * range_value
    * boolean_value
    * text_value
    * created_at
    * cot_updated_at
    """
    __tablename__ = 'retailer_attribute_mappings'
    __my_table_args__ = (
        UniqueConstraint(
            'dome_identifier',
            'retailer_code',
            'principal_code',
            'retailer_attribute_code',
	    name='retailer_attribute_mappings_unique_key'),
        ForeignKeyConstraint(
            ['dome_identifier', 'retailer_code'],
            ['retailers.dome_identifier', 'retailers.code'],
            name='retailer_attribute_mappings_retailer_code_fkey',
            onupdate='cascade'),
        {})

    key_csv_columns = ['dome_identifier', 'retailer_code',
                       'principal_code', 'retailer_attribute_code']
    required_csv_columns = key_csv_columns

    all_csv_columns = ['dome_identifier', 'retailer_code', 'business_name',
		       'retailer_category_raw', 'status',
		       'retailer_attribute_code', 'range_value',
		       'boolean_value', 'text_value', 'created_at',
		       'cot_updated_at']

    dome_identifier = Column(String(50), nullable=False)

    retailer_code = Column(String(50), nullable=False)
    retailer = relationship('Retailer',
        primaryjoin="and_(RetailerAttributeMapping.retailer_code=="
                    "Retailer.code, "
                    "RetailerAttributeMapping.dome_identifier=="
                    "Retailer.dome_identifier)",
        foreign_keys="[RetailerAttributeMapping.retailer_code, RetailerAttributeMapping.dome_identifier]")

    principal_code = Column(String(20), nullable=False)
    retailer_attribute_code = Column(String(20), nullable=False)
    retailer_attribute = relationship('RetailerAttribute',
        primaryjoin="and_(RetailerAttributeMapping.principal_code=="
                    "RetailerAttribute.principal_code, "
                    "RetailerAttributeMapping.retailer_attribute_code=="
                    "RetailerAttribute.code)",
        foreign_keys="[RetailerAttributeMapping.principal_code, RetailerAttributeMapping.retailer_attribute_code]")

    range_value = Column(Integer)
    boolean_value = Column(Boolean)
    text_value = Column(String(255))

    def __repr__(self):
        return '<RetailerAttributeMapping %s for %s>' %\
                   (self.retailer_attribute_code, self.retailer)

    def __before_update__(self, session, inner_session, now):
        super(RetailerAttributeMapping, self).__before_update__(
            session, inner_session, now)

        self.cot_updated_at = now
        self.cot_updated_by = self.__class__.get_username()

    @classmethod
    def form_create(cls, fields):
        new = cls()
        for key, value in fields.iteritems():
            setattr(new, key, value)
        model.Session.add(new)
        return new

    def form_update(self, fields):
        for key, value in fields.iteritems():
            setattr(self, key, value)

    @classmethod
    def get_by_or_init(cls, if_new_set={}, **params):
        """Call get_by; if no object is returned, initialize an
        object with the same parameters. But never add the object
	to session"""
        instance = cls.get_by(**params)
        if instance is None:
            instance = cls(**params)
        return instance
