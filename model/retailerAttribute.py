from datetime import timedelta
from sqlalchemy import func
from sqlalchemy.orm import relation
from sqlalchemy.schema import Column, ForeignKey, UniqueConstraint
from sqlalchemy.types import Boolean, String, DateTime

from atommodel.model import Base, StateEntity
from atommodel.model.util import get_model
from atommodel.model.mixins.basic_export import BasicExport

from operator import attrgetter
from lxml import etree

model = get_model()


class RetailerAttribute(Base, StateEntity, BasicExport):
    """
 ===========================
  **Options for the field defined in retailer_attributes**
 ===========================
    * principal_code(required, char(20))
    * code(required, char(20))
    * description(required, char(50))
    * input_type(required, char(20))
    * in_use(required, boolean)
    """

    INPUT_TYPE_RANGE = 'Range'
    INPUT_TYPE_BOOLEAN = 'Boolean'
    INPUT_TYPE_SELECT_DROP_DOWN = 'SelectDropDown'
    INPUT_TYPE_FREE_TEXT = 'FreeText'

    ATTRIBUTE_CODE_DISTNOTE = 'DISTNOTE'
    ATTRIBUTE_CODE_REMARKS = 'REMARKS'

    input_type_constants = (INPUT_TYPE_RANGE,
                            INPUT_TYPE_BOOLEAN,
                            INPUT_TYPE_SELECT_DROP_DOWN,
                            INPUT_TYPE_FREE_TEXT)

    __tablename__ = 'retailer_attributes'
    __table_args__ = (
        UniqueConstraint('principal_code',
                         'code',
                         name='retailer_attributes_unique_key'), {}, )

    key_csv_columns = ['principal_code', 'code']
    required_csv_columns = key_csv_columns + [
        'description', 'input_type', 'in_use']
    all_csv_columns = required_csv_columns

    principal_code = Column(
            String(20),
            ForeignKey('principals.code',
                       name='retailer_attributes_principal_code_fkey',
                       onupdate='cascade'),
            nullable=False)
    principal = relation('Principal', backref='retailer_attributes')
    code = Column(String(20), nullable=False)
    description = Column(String(50), nullable=False)
    input_type = Column(String(20), nullable=False)
    in_use = Column(Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<RetailerAttribute %s description %s>' %\
            (self.code, self.description)

    @classmethod
    def get_fake_reader(cls, reader):
        for row in reader:
            yield {'principal_code': row['principal_code'],
                   'lookup_name': row['lookup_name'],
                   'code': row['code'],
                   'description': row['description']}

    @classmethod
    def setup_validators(cls, **kwargs):
        from atommodel.schemas.lookupHeader import LookupItemCsvImport
        cls.import_schema = LookupItemCsvImport

    def __before_update__(self, session, inner_session, now):
        super(RetailerAttribute, self).__before_update__(
            session, inner_session, now)

        self.cot_updated_at = now
        self.cot_updated_by = self.__class__.get_username()

    @classmethod
    def form_create(cls, fields):
        new = cls()
        for key, value in fields.iteritems():
            setattr(new, key, value)
        if new.code in (cls.ATTRIBUTE_CODE_DISTNOTE,
                        cls.ATTRIBUTE_CODE_REMARKS):
            setattr(new, 'in_use', True)
        model.Session.add(new)
        return new

    def form_update(self, fields):
        for key, value in fields.iteritems():
            setattr(self, key, value)
        if self.code in (self.ATTRIBUTE_CODE_DISTNOTE,
                         self.ATTRIBUTE_CODE_REMARKS):
            setattr(self, 'in_use', True)

    def form_params(self, **kwargs):
        fields = ('code', 'description', 'input_type', 'in_use')
        params = dict(zip(fields, attrgetter(*fields)(self)))
        params.update(kwargs)
        return params

    def to_xml(self):
        LCA_NAMESPACE = "http://www.lcalink.com/ns"
        retailer_attribute_element = etree.Element(
            "{%s}%s" % (LCA_NAMESPACE, "retailer_attribute"),
            nsmap={"lca": LCA_NAMESPACE})
        attributes_for_xml = ('principal_code', 'code', 'description',
                              'input_type', 'in_use')
        for attribute in attributes_for_xml:
            value = getattr(self, attribute, None)
            if value is None:
                value = ""
            retailer_attribute_element.attrib[attribute] = unicode(value)

	if self.code == 'DISTNOTE':
            with open('/var/lib/cot/DISTNOTE_options.txt') as f:
		content = f.readlines()
	    content = [x.strip() for x in content]
	    content = [x for x in content if x]
            options = ','.join(content)
	    retailer_attribute_element.attrib['value'] = unicode(options)
        return retailer_attribute_element
