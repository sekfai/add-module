from cot.lib.util import Log

from sqlalchemy.orm import relation
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, UniqueConstraint
from sqlalchemy.schema import ForeignKey
from sqlalchemy.types import Integer, String, Boolean, Date
from sqlalchemy.sql import and_

from atommodel.model import Base, AuditEntity
from atommodel.model.mixins.basic_export import BasicExport
from atommodel.model.mixins.basic_import import BasicImport
from atommodel.model.util import get_model

from datetime import datetime
from operator import attrgetter

log = Log(__name__)
model = get_model()


class RetailerAttributeCategory(Base, AuditEntity,
                                BasicImport, BasicExport):
    """
 ===========================
  **Mapping between retailer attribute and retailer category**
 ===========================
    * principal_code(required, char(20))
    * retailer_category_id(required, integer)
    * retailer_attribute_code(required, char(20))
    """
    __tablename__ = 'retailer_attribute_categories'
    __my_table_args__ = (
        UniqueConstraint(
            'retailer_category_id',
	    'principal_code',
            'retailer_attribute_code',
            'valid_from',
            'valid_to',
	    name='retailer_attribute_categories_unique_key'),
        {},
    )

    key_csv_columns = ['retailer_category_raw', 'principal_code',
                       'retailer_attribute_code', 'valid_from', 'valid_to']
    required_csv_columns = key_csv_columns +\
	['min_value', 'max_value', 'bool_value']
    all_csv_columns = required_csv_columns

    principal_code = Column(String(20), nullable=False)

    retailer_category_id = Column(
	Integer,
	ForeignKey('retailer_categories.id',
		   name='retailer_attribute_categories_retailer_category_id_fkey',
		   onupdate='cascade'),
        index=True, nullable=False)

    retailer_category = relation('RetailerCategory',
		                 backref='retailer_attribute_categories')

    retailer_attribute_code = Column(String(20), nullable=False)
    retailer_attribute = relationship('RetailerAttribute',
        primaryjoin="and_(RetailerAttributeCategory.principal_code=="
                    "RetailerAttribute.principal_code, "
                    "RetailerAttributeCategory.retailer_attribute_code=="
                    "RetailerAttribute.code)",
        foreign_keys="[RetailerAttributeCategory.principal_code, RetailerAttributeCategory.retailer_attribute_code]")


    min_value = Column(Integer)
    max_value = Column(Integer)
    bool_value = Column(Boolean)
    valid_from = Column(Date, nullable=False)
    valid_to = Column(Date, nullable=False)

    def __repr__(self):
        return '<RetailerAttributeCategory %s RetailerCategory %s>' %\
            (self.retailer_attribute_code, self.retailer_category)

    def to_dropdown(self):
        return self.to_root(format='%(code)s',
                            leaf_format='%(code)s (%(name)s)')

    @classmethod
    def setup_validators(cls, **kwargs):
        from atommodel.schemas.retailerAttributeCategory import \
            RetailerAttributeCategoryCsvImport
        cls.import_schema = RetailerAttributeCategoryCsvImport

    def __before_update__(self, session, inner_session, now):
        super(RetailerAttributeCategory, self).__before_update__(
            session, inner_session, now)

    @classmethod
    def form_create(cls, fields):
        new = cls()
        for key, value in fields.iteritems():
            setattr(new, key, value)
        model.Session.add(new)
        return new

    def form_update(self, fields):
        for key, value in fields.iteritems():
            setattr(self, key, value)

    def form_params(self, **kwargs):
        fields = ('retailer_attribute_code', 'min_value',
		  'max_value', 'bool_value', 'valid_from', 'valid_to')
        params = dict(zip(fields, attrgetter(*fields)(self)))
	principal = model.Principal.get_by(code=self.principal_code)
        params['principal'] = principal.id
        params['retailer_category'] = self.retailer_category_id
        params.update(kwargs)
        return params

    @classmethod
    def publish_all(cls, lookups_domes_id):
        for id in lookups_domes_id:
            query = cls.query.filter_by(id=id)
            instance = query.one()
            if instance.publishable is True:
                instance.published = True
            else:
                raise Exception("%s is not publishable. "
                                "Transaction has been rolled back." %
                                query.one())

    @classmethod
    def unpublish_all(cls, lookups_domes_id):
        cls.query.filter(cls.id.in_(lookups_domes_id))\
            .update({"published": False}, synchronize_session=False)

    @classmethod
    def save_from_csv_row(cls, row, **kwargs):
        values = cls.validate_row(row, **kwargs)

        params = {}
        for column in cls.key_csv_columns:
            if column == 'retailer_category_raw':
                continue
            params[column] = values.get(column)

        params['retailer_category_id'] = values['retailer_category_id']
        entry = cls.get_by_or_init(**params)

        for column in set(cls.all_csv_columns).difference(cls.key_csv_columns):
            setattr(entry, column, values[column])

        entry.retailer_category = values['retailer_category']
        entry.retailer_attribute = values['retailer_attribute']

    @classmethod
    def export_to_csv_query(cls, filter=None, **kwargs):
        columns = [x for x in cls.key_csv_columns if
                   x != 'retailer_category_raw']
        return cls.query.filter(filter).order_by(*columns)

    def to_csv_dict(self, custom_field_values=None):
        return_dict = super(RetailerAttributeCategory, self)\
            .to_csv_dict(custom_field_values)

        # set to valid atomstore retailer category
        # instead of using dms retailer category raw column value
        # which might not exist in cot.

        return_dict['retailer_category_raw'] = \
            self.retailer_category.to_raw_string() \
            if self.retailer_category is not None else None
        return return_dict
