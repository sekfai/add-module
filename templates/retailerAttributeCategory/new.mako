<%inherit file='/base.mako'/>
<%namespace file='fields.mako' name='fields' import='*'/>
<%namespace file='/error.mako' name='error' import='*'/>
<%namespace file='/form.mako' name='form' import='*'/>

<div class="title-bar">
    <h2>New Retailer Attribute Category</h2>
</div>

<div class='container-center'>
    ${form.render_new_form(fields)}
</div>
