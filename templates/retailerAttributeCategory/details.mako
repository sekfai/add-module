<div class='row'>
    <label>Principal</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.principal_code}</label>
</div >

<div class='row'>
    <label>Retailer Category</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.retailer_category.to_raw_string()}</label>
</div >

<div class='row'>
    <label>Retailer Attribute Code</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.retailer_attribute_code}</label>
</div >

<div class='row'>
    <label>Description</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.retailer_attribute.description}</label>
</div >

<div class='row'>
    <label>Input Type</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.retailer_attribute.input_type}</label>
</div >

% if c.instance.retailer_attribute.input_type == 'Range':
<div class='row'>
    <label>Min</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.min_value}</label>
</div >

<div class='row'>
    <label>Max</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.max_value}</label>
</div >

% elif c.instance.retailer_attribute.input_type == 'Boolean':
<div class='row'>
    <label>Value</label>
    <span>:</span>
    <label class='readonly-form-field'>
        % if c.instance.bool_value:
	Yes
	% else:
	No
	% endif
    </label>
</div >
% endif

<div class='row'>
    <label>Valid from</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.valid_from}</label>
</div >

<div class='row'>
    <label>Valid to</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.valid_to}</label>
</div >
