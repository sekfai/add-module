<%inherit file='/base.mako'/>
<%namespace name='error' file='/error.mako'/>

<div class="title-bar">
    <div class="control-wrapper">
        ${h.link_to('Retailer Attribute Categories',h.url('retailerAttributeCategories'), class_="boxy view-form")}
    </div>
    <h2>Retailer Attribute Category - ${c.id} </h2>${h.link_to(' ', h.url('edit_%s' % c.member, id=c.id), title='Edit', class_='edit')}
</div>

<div class='container-center'>
    <div style='clear: both;'></div>
    <div class='filter-wrapper'>
        <%include file='details.mako'/>
    </div>

    <div id='input_fields'>
    </div>
</div>
