<table class='grid'>
    <thead>
        <tr>
            <th></th>
            <th>Retailer Category</th>
            <th>${h.sort('principal_code', 'Principal') | n}</th>
            <th>${h.sort('retailer_attribute_code', 'Retailer Attribute Code') | n}</th>
            <th>Value</th>
            <th>In Use</th>
            <th>${h.sort('valid_from', 'Valid from') | n}</th>
            <th>${h.sort('valid_to', 'Valid to') | n}</th>
        </tr>
    </thead>
    <tbody>
	<% i=0 %>
        % for i, instance in enumerate(c.instances):
	    ${create_rows(i, instance)}
        % endfor
    </tbody>
</table>

${c.page.pager('~2~', onclick="$('#pagearea').load('%s',bindSelectAllClickEvent); return false;", **c.linkargs)}
(Total: ${c.page.item_count})

<%def name='create_rows(i, instance)'>\
    <tr class="${i % 2 and 'odd' or 'even'}">
        <td>
	<div class='actions'>
        ${h.link_to(' ', h.url(c.member, id=instance.id), title='View', class_='view')}
        ${h.link_to(' ', h.url('edit_%s' % c.member, id=instance.id), title='Edit', class_='edit')}
	</div>
        </td>
        <td>${instance.retailer_category.to_raw_string()}</td>
        <td>${instance.principal_code}</td>
        <td>${instance.retailer_attribute_code}</td>
        <td>
        % if instance.retailer_attribute.input_type == 'Range':
        ${instance.min_value} ~ ${instance.max_value}
        % elif instance.retailer_attribute.input_type == 'Boolean':
            % if instance.bool_value:
            Yes
            % else:
            No
            % endif
        % endif
	</td>
        <td>${instance.retailer_attribute.in_use}</td>
        <td>${instance.valid_from}</td>
        <td>${instance.valid_to}</td>
</%def>

<div style="display:block; padding:10px 0; text-align:center;"><a href="#top">back to top</a></div>
