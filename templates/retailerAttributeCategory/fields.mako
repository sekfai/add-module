<div class='container-center'>
    <form:error name='form'/>
    <div class='filter-wrapper'>
        <div class='row'>
            <label for='principal'>Principal</label>
            ${h.select(name='principal', selected_values=[], options=c.principal_options)}
        </div>

        <div class='row'>
            <label for='retailer_category'>Retailer Category</label>
            <select name="retailer_category" id="retailer_category"></select>
	    ${h.hidden(name='current_retailer_category_id')}
        </div>

        <div class='row'>
            <label for='retailer_attribute_code'>Retailer Attribute Code</label>
            ${h.select(name='retailer_attribute_code', selected_values=[], options=c.retailer_attribute_code_options)}
	    ${h.hidden(name='current_retailer_attribute_code')}
        </div>

        <div class='row'>
            <label for='description'>Description</label>
            <label id='description' class='readonly-form-field' style='width:350px'></label>
        </div>

        <div class='row'>
            <label for='input_type'>Input Type</label>
            <label id='input_type' class='readonly-form-field' style='width:350px'></label>
        </div>

	<div class='row' id='range_min'>
	    <label for='min_value'>Min</label>
	    ${h.text(name='min_value', class_='text', style='width:90px')}
	</div>

	<div class='row' id='range_max'>
	    <label for='max_value'>Max</label>
	    ${h.text(name='max_value', class_='text', style='width:90px')}
	</div>

	<div class='row' id='boolean'>
	    <label for='bool_value'>Value</label>
	    ${h.select(name='bool_value', selected_values=[], options=[('False', 'No'), ('True', 'Yes'), ('', '')])}
	</div>

        <div class='row'>
            <label for='height'>Valid From</label>
            ${h.text(name='valid_from', class_='text datepicker')}
        </div>

        <div class='row'>
            <label for='height'>Valid To</label>
            ${h.text(name='valid_to', class_='text datepicker')}
        </div>
        ${h.submit(name='action', value='Submit')}
    </div>
</div>
