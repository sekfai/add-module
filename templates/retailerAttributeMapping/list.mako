<br />
<br clear="all">
<table class='grid'>
    <thead>
        <tr>
            <th></th>

            % if c.collection == 'retailerAttributeMappings':
                <th>${h.sort('dome_identifier', 'Dome Identifier')}</th>
                <th>${h.sort('retailer_code', 'Retailer Code')}</th>
                <th>Business Name</th>
                <th>Retailer Created At</th>
                <th>${h.sort('cot_updated_at', 'Last Updated')}</th>
                <th>Status</th>
                <th>Active</th>
            % else:
                <th>Dome Identifier</th>
                <th>Retailer Code</th>
                <th>Business Name</th>
                <th>Retailer Created At</th>
                <th>Last Updated</th>
                <th>Status</th>
                <th>Active</th>
            % endif
        </tr>
    </thead>
    <tbody>
        <%
            if c.member=='retailerAttributeMapping':
                c.retailer_attribute_mappings = c.instances
        %>
        % for i, instance in enumerate(c.retailer_attribute_mappings):
            <tr class="${i % 2 and 'odd' or 'even'}">
                <td>
                    <div class='actions'>
                    ${h.link_to(' ', h.url('view_%s' % c.collection, dome_identifier=instance.dome_identifier, retailer_code=instance.retailer_code, cot_updated_at=instance.cot_updated_at), class_='view')}
                    % if c.distributors_data_editable == True:
                        ${h.link_to(' ', h.url('edit_%s' % 'retailerPrincipal', id=instance.id), title='Edit', class_='edit')}
                    % endif
                    </div>
                </td>

                <td>${h.human_readable_dome_identifier(instance.retailer.dome)}</td>

                <td>${instance.retailer.code}</td>

                <td>${instance.retailer.business_name}</td>

                <td>${instance.retailer.created_at}</td>

                <td>${instance.cot_updated_at}</td>

                <td>${instance.retailer.status}</td>

                <td>${instance.retailer.active}</td>

            </tr>
        % endfor
    </tbody>
</table>
% if c.collection == 'retailerAttributeMappings':
    ${c.page.pager('~2~', onclick="$('#pagearea').load('%s'); return false;", **c.linkargs)}
    (Total: ${c.page.item_count})
    <div style="display:block; padding:10px 0; text-align:center;"><a href="#top">back to top</a></div>
% endif
