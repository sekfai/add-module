<%inherit file='/base.mako'/>

<div class="title-bar">
	<div class="control-wrapper"></div>
	<h2>Retailer-Attribute Mapping - ${c.retailer.code} </h2>
</div>

<div class='container-center'>
	<div class='filter-wrapper'>
	    <%include file='details.mako'/>
	</div>
</div>
