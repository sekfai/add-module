<%
    # workaround due to http://www.makotemplates.org/trac/ticket/3
    # discussion at http://markmail.org/message/nt23dknf3ogi2aq5
    h = context.get('h')
%>

<div class='row'>
    <label>Dome Identifier</label>
    <span>:</span>
    <label class='readonly-form-field'> ${h.human_readable_dome_identifier(c.retailer.dome)}</label>
</div >

<div class='row'>
    <label>Retailer Code</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.retailer.code}</label>
</div >

<div class='row'>
    <label>Status</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.retailer.status}</label>
</div >

<div class='row'>
    <label>Business Name</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.retailer.business_name}</label>
</div >

<div class='row'>
    <label>Retailer Created At</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.retailer.created_at}</label>
</div >

<div class='row'>
    <label>Last Updated</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.last_updated}</label>
</div >

<div class='row'>
    <label>Retailer Category Raw</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.retailer_category_raw}</label>
</div >

% for instance in c.retailer_attribute_mappings:
<div class='row'>
    <label>${instance.retailer_attribute_code}</label>
    <span>:</span>
    % if instance.retailer_attribute.input_type == 'Range':
    <label class='readonly-form-field'> ${instance.range_value}</label>
    % elif instance.retailer_attribute.input_type == 'Boolean':
        % if instance.boolean_value:
        <label class='readonly-form-field'> Yes</label>
	% else:
        <label class='readonly-form-field'> No</label>
	% endif
    % else:
    <label class='readonly-form-field'> ${instance.text_value}</label>
    % endif
</div >
% endfor
