<div class='container-center'>
    <form:error name='form'/>
    <div class='filter-wrapper'>

    <div class='row'>
        <label for='principal'>Principal</label>
        % if c.action=='new' or c.action=='create':
            ${h.select(name='principal', selected_values=[], options=c.principal_options)}
        % else:
            <label class='readonly-form-field' id='principal'> ${c.instance.principal_code}</label>
        % endif
    </div>

    <div class='row'>
        <label for='code'>Attribute Code</label>
        % if c.action=='new' or c.action=='create':
            ${h.text(name='code', id='code', maxlength=20, class_='text')}
        % else:
            <label class='readonly-form-field' id='code'> ${c.instance.code}</label>
        % endif
    </div>

    <div class='row'>
        <label for='description'>Description</label>
        ${h.text(name='description', id='description', maxlength=50, class_='text')}
    </div>

    <div class='row'>
        <label for='input_type'>Input Type</label>
        % if c.action=='new' or c.action=='create':
            ${h.select(name='input_type', selected_values=[], options=c.input_type_options)}
	% else:
            <label class='readonly-form-field' id='input_type'> ${c.instance.input_type}</label>
	% endif
    </div>

    <div class='row'>
        <label for='in_use'>In Use</label>
        ${h.checkbox(name='in_use', value='True')}
    </div>

    <div>
    ${h.submit(name='action', value='Submit')}
    </div>

    </div>
</div>
