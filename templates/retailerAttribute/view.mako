<%inherit file='/base.mako'/>
<%namespace name='error' file='/error.mako'/>

<div class="title-bar">
    <div class="control-wrapper">
        ${h.link_to('Retailer Attribute',h.url('retailerAttributes'), class_="boxy view-form")}
    </div>
    <h2 class='main'>Retailer Attribute - ${c.instance.code} </h2>
    ${h.link_to(' ', h.url('edit_%s' % c.member, id=c.id), title='Edit', class_='edit')}
</div>

<div class='container-center'>
    <div class='filter-wrapper'>
        <%include file='details.mako'/>
    </div>
</div>
