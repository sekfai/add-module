<table class='grid'>
    <thead>
        <tr>
            <th></th>
            <th>${h.sort('principal_code', 'Principal') | n}</th>
            <th>${h.sort('code', 'Attribute Code') | n}</th>
            <th>${h.sort('description', 'Description') | n}</th>
            <th>${h.sort('input_type', 'Input Type') | n}</th>
            <th>${h.sort('in_use', 'In Use') | n}</th>
        </tr>
    </thead>
    <tbody>
    <% i=0 %>
        % for i, instance in enumerate(c.instances):
        ${create_rows(i, instance)}
        % endfor
    </tbody>
</table>

<%def name='create_rows(i, instance)'>\
    <tr class="${i % 2 and 'odd' or 'even'}">
        <td>
    <div class='actions'>
        ${h.link_to(' ', h.url(c.member, id=instance.id), title='View', class_='view')}
        ${h.link_to(' ', h.url('edit_%s' % c.member, id=instance.id), title='Edit', class_='edit')}
    </div>
        </td>
        <td>${instance.principal_code}</td>
        <td>${instance.code}</td>
        <td>${instance.description}</td>
        <td>${instance.input_type}</td>
        <td>${instance.in_use}</td>
    </tr>
</%def>

${c.page.pager('~2~', onclick="$('#pagearea').load('%s'); return false;", **c.linkargs)}
(Total: ${c.page.item_count})

<div style="display:block; padding:10px 0; text-align:center;"><a href="#top">back to top</a></div>
