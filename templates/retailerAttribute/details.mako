<div class='row'>
    <label>Principal</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.principal_code}</label>
</div >

<div class='row'>
    <label>Attribute Code</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.code}</label>
</div >

<div class='row'>
    <label>Description</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.description}</label>
</div >

<div class='row'>
    <label>Input Type</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.input_type}</label>
</div >

<div class='row'>
    <label>In Use</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.in_use}</label>
</div >

<div class='row'>
    <label>Created date at</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.cot_created_at}</label>
</div >

<div class='row'>
    <label>Last updated date</label>
    <span>:</span>
    <label class='readonly-form-field'> ${c.instance.cot_updated_at}</label>
</div >
