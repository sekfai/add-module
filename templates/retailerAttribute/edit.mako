<%inherit file='/base.mako'/>
<%namespace file='fields.mako' name='fields' import='*'/>
<%namespace file='/error.mako' name='error' import='*'/>
<%namespace file='/form.mako' name='form' import='*'/>

<div class="title-bar">
    <h2>Retailer Attribute - ${c.instance.code}</h2>
</div>

<div class="container-center">
    ${form.render_edit_form(fields)}
</div>
<br/>
