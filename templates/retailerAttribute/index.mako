<%inherit file='/base.mako'/>
<div class="title-bar">
    <div class="control-wrapper">
        ${h.context_links()}
    </div>
    <h2>${c.header}</h2>
</div>

<div class='container-center'>
    <div class='filter-wrapper'>
        ${c.topForm | n}
        ${h.filter_area() | n}
    </div>
    <div id='pagearea'>
        <%include file='list.mako'/>
    </div>
</div>
