import logging

from operator import itemgetter
from pylons import request, response
from pylons import tmpl_context as c

from atommodel.schemas.retailerAttributeCategory import CreateRetailerAttributeCategory
from atommodel.schemas.retailerAttributeCategory import UpdateRetailerAttributeCategory

from cotpanel import model
from cotpanel.lib.rest import RestController
from cotpanel.lib.upload import UploadMixin

import simplejson as json
from sqlalchemy import or_

log = logging.getLogger(__name__)


class RetailerattributecategoryController(RestController, UploadMixin):
    member = 'retailerAttributeCategory'
    collection = 'retailerAttributeCategories'
    entity = model.RetailerAttributeCategory
    create_schema = CreateRetailerAttributeCategory
    update_schema = UpdateRetailerAttributeCategory
    page_size = 50

    csv_save_args = dict(persevere=True)

    filters = [
        ('principal', 'Principal', 'select', 'False'),
        ('retcat', 'Retailer Category', 'select', 'False'),
        ('retailer_attribute_code', 'Retailer Attribute Code', 'select', 'False'),
        ('date', 'Valid From', 'daterange', 'False')]

    filter_columns = [('principal', 'principal_code'),
                      ('retcat', 'retailer_category_id'),
                      ('retailer_attribute_code', 'retailer_attribute_code'),
                      ('date', 'valid_from')]

    allow_create_new_record = True
    allow_csv_upload = True
    allow_batch_edit = False
    allow_batch_new = False

    def index(self, format='html'):
        c.header = 'Retailer Attribute Category'
        return self._index(format=format, filter=self._filter_column(),
                           order_by=['cot_updated_at desc'])

    def _filter_column(self):
        clause = []

        if c.linkargs == '':
            c.linkargs = {}

        date_froms = request.params.getall('datefrom')
        date_tos = request.params.getall('dateto')
        dates = zip(date_froms, date_tos)

        if dates:
            for date_from, date_to in dates:
                if date_from and date_to:
                    clause.append("valid_from>='%s' AND valid_from<='%s'"
                                  % (date_from, date_to))
                else:
                    if date_from:
                        clause.append("valid_from>='%s'" % date_from)
                    if date_to:
                        clause.append("valid_from<='%s'" % date_to)
            c.linkargs['dateto'] = date_tos
            c.linkargs['datefrom'] = date_froms

        clause = [or_(*clause)]
        return RestController._filter_column(self, clause=clause)

    def show(self, id, format="html"):
        c.lookup_dome = c.instance
        return RestController.show(self, id, format)

    def filter_retcat_options(self):
        ret_cats = model.RetailerCategory.query
        category_options = [[x.id, x.to_raw_string()]
                           for x in ret_cats
                           if x.to_raw_string().count(' > ') == 2]
        category_options = sorted(category_options, key=itemgetter(1))
        category_options_json = json.dumps(category_options, indent=4)
        return category_options_json

    def filter_retailer_attribute_code_options(self):
	# this input type need to be excluded because it doesnt have target
	select_drop_down = model.RetailerAttribute.INPUT_TYPE_SELECT_DROP_DOWN
	free_text = model.RetailerAttribute.INPUT_TYPE_FREE_TEXT
        retailer_attributes = model.RetailerAttribute.query.filter(
            model.RetailerAttribute.input_type!=select_drop_down).filter(
                model.RetailerAttribute.input_type!=free_text)
        attribute_code_options = [
            (x.code, x.code) for x in retailer_attributes]
        attribute_code_options = set(attribute_code_options)
        attribute_code_options = sorted(attribute_code_options,
			                key=itemgetter(1))
        attribute_code_options_json = json.dumps(attribute_code_options,
			                         indent=4)
        return attribute_code_options_json

    def populate_retailercategory_options(self):
        principal = model.Principal.query.filter_by(id=request.params.get(
            'principal_id')).one()
        ret_cats = model.RetailerCategory.query.filter_by(
            principal_code=principal.code)
        category_options = [[x.id, x.to_raw_string()] for x in ret_cats
                           if x.to_raw_string().count(' > ') == 2]
        category_options = sorted(category_options, key=itemgetter(1))
        category_options_json = json.dumps(category_options, indent=4)
	return category_options_json

    def populate_retailerattributecode_options(self):
	# this input type need to be excluded because it doesnt have target
	select_drop_down = model.RetailerAttribute.INPUT_TYPE_SELECT_DROP_DOWN
	free_text = model.RetailerAttribute.INPUT_TYPE_FREE_TEXT
        principal = model.Principal.query.filter_by(id=request.params.get(
            'principal_id')).one()
        retailer_attributes = model.RetailerAttribute.query.filter_by(
            principal_code=principal.code).filter(
		model.RetailerAttribute.input_type!=select_drop_down).filter(
		    model.RetailerAttribute.input_type!=free_text)
        attribute_code_options = [
            [x.code, x.code] for x in retailer_attributes]
        attribute_code_options = sorted(attribute_code_options,
			                key=itemgetter(1))
        attribute_code_options_json = json.dumps(attribute_code_options,
			                         indent=4)
        return attribute_code_options_json

    def get_retailer_attribute(self):
        principal = model.Principal.query.filter_by(id=request.params.get(
            'principal_id')).one()
        retailer_attribute = model.RetailerAttribute.query.filter_by(
                code=request.params.get('retailer_attribute_code'),
		principal_code=principal.code).one()
        retailer_attribute_json = json.dumps({
            'description': retailer_attribute.description,
            'input_type': retailer_attribute.input_type,
            })
        return retailer_attribute_json

    def _populate_options(self):
        principals = model.Principal.query.filter(
            model.Principal.filterFromPermission()).order_by('id')
        c.principal_options = [[x.id, x.code] for x in principals]

    def _new_params(self):
        params = super(RetailerattributecategoryController, self)._new_params()
        if 'retailer_category' in params:
            params['current_retailer_category_id'] = params['retailer_category']
	if 'retailer_attribute_code' in params:
	    params['current_retailer_attribute_code'] =\
                params['retailer_attribute_code']
        return params

    def _edit_params(self, instance):
        params = super(RetailerattributecategoryController, self).\
			_edit_params(instance)
        params['current_retailer_category_id'] = params['retailer_category']
	params['current_retailer_attribute_code'] =\
            params['retailer_attribute_code']
        return params
