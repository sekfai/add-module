from operator import itemgetter
from cotpanel.lib.rest import RestController
from pylons import tmpl_context as c
from cotpanel import model
from atommodel.schemas.retailerAttribute import CreateRetailerAttribute
from atommodel.schemas.retailerAttribute import UpdateRetailerAttribute
import simplejson as json
from pylons import request
from pylons import response
import logging
log = logging.getLogger(__name__)


class RetailerattributeController(RestController):
    member = 'retailerAttribute'
    collection = 'retailerAttributes'
    entity = model.RetailerAttribute
    create_schema = CreateRetailerAttribute
    update_schema = UpdateRetailerAttribute
    page_size = 20
    allow_create_new_record = True
    allow_csv_upload = False

    filters = [('principal', 'Principal', 'select', 'False'),
               ('attribute_code', 'Attribute Code', 'select', 'False'),
               ('input_type', 'Input Type', 'select', 'False'),
               ('in_use', 'In Use', 'select', 'False')]

    filter_columns = [('principal', 'principal_code'),
                      ('attribute_code', 'code'),
                      ('input_type', 'input_type'),
                      ('in_use', 'in_use')]

    def index(self, format='html'):
        c.header = 'Retailer Attributes'
        return self._index(format=format, filter=self._filter_column(),
                           order_by=['principal_code', 'code'])

    def _populate_options(self):
        c.principal_options = [('', '')] +\
            [(p.id, p.code)
                for p in model.Principal.query.filter(
                    model.Principal.filterFromPermission())]
        c.input_type_options = sorted([[x, x] for x in \
	        self.entity.input_type_constants])

    def filter_attribute_code_options(self):
	query = model.Session.query(self.entity.code.distinct().label('code'))
        attribute_code_options = [('', '')] + \
            [[row.code, row.code] for row in query.all()]
        attribute_code_options = sorted(attribute_code_options,
			                key=itemgetter(1))
        attribute_code_options_json = json.dumps(attribute_code_options,
			                         indent=4)
        return attribute_code_options_json

    def filter_input_type_options(self):
        c.input_type_options = [('', '')] + \
            [[x, x] for x in self.entity.input_type_constants]
        return json.dumps(c.input_type_options)

    def filter_in_use_options(self):
        c.in_use_options = [('False', 'False'), ('True', 'True')]
        return json.dumps(c.in_use_options)
