import logging
import string

from pylons import request, response

from cotpanel import model
from cotpanel.lib.feed import FeedController
from pylons import url

import uuid
import pytz
import dateutil

from lxml import etree

log = logging.getLogger(__name__)


class RetailerattributefeedController(FeedController):
    entity = model.DomeAttributeMapping

    def _get_feed_data(self, *args, **kwargs):
        feed = {}
        dome_identifier = kwargs['dome_identifier']
        last_modified = self.entity.get_last_updated_at_by_dome_identifier(
            dome_identifier)
        response.headers['Last-Modified'] = last_modified.astimezone(
            pytz.UTC).strftime("%a, %d %b %Y %H:%M:%S +0000")
        response.headers['X-LCALINK-Last-Modified'] = string.replace(
            last_modified.astimezone(pytz.UTC).strftime(
                "%a, %d %b %Y %H:%M:%S.<microseconds> +0000"),
            '<microseconds>',
            str(last_modified.astimezone(pytz.UTC).microsecond))
        feed['title'] = "%s Retailer Attribute Feed" % dome_identifier
        feed['id'] = uuid.uuid1().get_urn()
        feed['updated'] = last_modified.astimezone(pytz.UTC)
        feed['author'] = {
            'name': 'Logistics Consulting Asia Sdn Bhd (494849-W)',
            'uri': 'http://www.lcalink.com'
        }
        feed['link'] = url(
            'retailerAttributeFeeds',
            dome_identifier=dome_identifier,
            qualified=True)

        start_parameter = request.params.get('start', None)
        start = None
        if start_parameter is not None:
            start = dateutil.parser.parse(
                start_parameter).astimezone(dateutil.tz.tzlocal())
        entries = []

        entities = self.entity.get_dome_attribute_mapping_for_feed(
	    dome_identifier, start)
        for dome_attribute_mapping, max_cot_updated_at in entities:
            if start is None:
	        # send all retailer attributes due to unpublish + republish
		value=etree.tostring(
		    dome_attribute_mapping.to_xml(),
		    pretty_print=True)
            elif dome_attribute_mapping.cot_updated_at > start:
	        # send all retailer attributes due to unpublish + republish
		value=etree.tostring(
		    dome_attribute_mapping.to_xml(),
		    pretty_print=True)
	    else:
	        # send partial retailer attributes updated at published state
                value=etree.tostring(
                    dome_attribute_mapping.to_xml(start=start),
                    pretty_print=True)

            entries.append(
                dict(
                    title=dome_attribute_mapping.principal_code + ' - '
                    + dome_attribute_mapping.lookup_name,
                    id=dome_attribute_mapping.atom_identifier,
                    summary='Retailer Attribute',
                    published=dome_attribute_mapping.cot_created_at.astimezone(
                        pytz.UTC),
                    updated=dome_attribute_mapping.cot_updated_at.astimezone(
                        pytz.UTC),
                    link=url('domeAttributeMapping',
			     id=dome_attribute_mapping.id, qualified=True),
                    content=dict(
                        type='application/xml',
                        value=value)
                ))
        feed['entries'] = entries

        return feed
