import logging
from datetime import datetime, timedelta

from pylons import config
from pylons import request, response, session
from pylons import tmpl_context as c
from pylons.controllers.util import abort, redirect
from pylons import url
from sqlalchemy import func, and_, or_
from sqlalchemy.sql.functions import coalesce

from cotpanel import model
from cotpanel.lib.rest import RestController, Export

import simplejson as json

log = logging.getLogger(__name__)


class RetailerattributemappingController(RestController):
    member = 'retailerAttributeMapping'
    collection = 'retailerAttributeMappings'
    entity = model.RetailerAttributeMapping

    allow_csv_upload = False
    allow_create_new_record = False

    distributors_data_editable = config['distributors_data_editable']

    filters = [('dome', 'Dome', 'select', 'False'),
               ('retailer', 'Retailer', 'text', 'False'),
	       ('business_name', 'Business Name', 'text', 'False'),
               ('status', 'Status', 'text', 'False'),
               ('active', 'Active', 'select', 'False'),
               ('last_updated', 'Last Updated', 'daterange', 'False'),
               ('retailer_created_at', 'Retailer Created At', 'daterange', 'False')]

    filter_columns = [('dome', 'dome_identifier'),
                      ('retailer', 'retailer_code'),
                      ('business_name', 'business_name'),
		      ('status', 'status'),
		      ('active', 'active'),
                      ('last_updated', 'cot_updated_at'),
                      ('retailer_created_at', 'created_at')]

    def index(self, format='html'):
        c.header = 'Retailer Attribute Mappings'
        return self._index(format=format, filter=self._filter_column())

    def _filter_column(self):
        clause = []

        if c.linkargs == '':
            c.linkargs = {}

        date_froms = request.params.getall('retailer_created_atfrom')
        date_tos = request.params.getall('retailer_created_atto')
        dates = zip(date_froms, date_tos)

        if dates:
            for date_from, date_to in dates:
                if date_from and date_to:
                    date = datetime.strptime(date_to, '%Y-%m-%d')
                    modified_date = date + timedelta(days=1)
                    new_date_to = datetime.strftime(modified_date, '%Y-%m-%d')
                    clause.append("retailers.created_at>='%s' AND retailers.created_at<'%s'"
                                  % (date_from, new_date_to))
                else:
                    if date_from:
                        clause.append("retailers.created_at>='%s'" % date_from)
                    if date_to:
                        date = datetime.strptime(date_to, '%Y-%m-%d')
                        modified_date = date + timedelta(days=1)
                        new_date_to = datetime.strftime(modified_date,
                                                        '%Y-%m-%d')
                        clause.append("retailers.created_at<'%s'" % new_date_to)
            c.linkargs['retailer_created_atto'] = date_tos
            c.linkargs['retailer_created_atfrom'] = date_froms

        clause = [or_(*clause)]
        return RestController._filter_column(self, clause=clause)

    def _populate_options(self):
        c.principal_options = [
            ('', '')] + [(x.code, x.code) for x in
                         model.Principal.query.filter(
                             model.Principal.filterFromPermission())]
        c.dome_identifier_options = [('', ''), ]

    def filter_active_options(self):
        active_options = [
            ('', '')] + [('True', 'True'), ('False', 'False')]
        return json.dumps(active_options)

    def view(self):
	dome_identifier = request.GET.getone('dome_identifier')
	retailer_code = request.GET.getone('retailer_code')
	cot_updated_at = request.GET.getone('cot_updated_at')

        c.retailer = model.Retailer.query.filter_by(
            dome_identifier=dome_identifier,
            code=retailer_code).one()

        retailer_principal = model.RetailerPrincipal.query.filter_by(
            dome_identifier=dome_identifier,
            retailer_code=retailer_code).first()

        if retailer_principal:
            c.retailer_category_raw = retailer_principal.retailer_category_raw
        else:
            principal_code = model.DomePrincipal.query.filter_by(
                dome_identifier=dome_identifier).one().principal_code
            root_retailer_category = model.RetailerCategory.query.filter_by(
                principal_code=principal_code).filter(
                    model.RetailerCategory.parent_id == None).one()
            c.retailer_category_raw = root_retailer_category.to_raw_string()

        c.retailer_attribute_mappings = self.entity.query.join(
	    model.RetailerAttribute,
	    and_(self.entity.principal_code ==\
	        model.RetailerAttribute.principal_code,
	        self.entity.retailer_attribute_code ==\
	        model.RetailerAttribute.code)
	    ).filter(
                and_(self.entity.dome_identifier==dome_identifier,
                     self.entity.retailer_code==retailer_code)
            ).order_by(
                model.RetailerAttribute.id)
        c.last_updated = cot_updated_at
	return self._render('view.mako')

    def export(self):
        fieldnames = self.entity.all_csv_columns
	filter = self._filter_column()
        export = Export(self.entity, filter=filter, fieldnames=fieldnames)

        csv_writer = self.entity.get_csv_writer(
            export.csv_file, fieldnames=fieldnames)

        query = self.entity.query.distinct('retailer_attribute_mappings.dome_identifier, retailer_attribute_mappings.retailer_code'
        ).order_by(model.RetailerAttributeMapping.dome_identifier,
                   model.RetailerAttributeMapping.retailer_code,
                   model.RetailerAttributeMapping.cot_updated_at.desc())

        query = query.join(model.Retailer,
            and_(model.RetailerAttributeMapping.dome_identifier ==\
                 model.Retailer.dome_identifier,
                 model.RetailerAttributeMapping.retailer_code ==\
                 model.Retailer.code))

        query = query.filter(filter)
        clause = []
        date_froms = request.params.getall('last_updatedfrom')
        date_tos = request.params.getall('last_updatedto')
        dates = zip(date_froms, date_tos)

        if dates:
            for date_from, date_to in dates:
                if date_from and date_to:
                    clause.append("cot_updated_at>='%s' AND cot_updated_at<='%s'"
                                  % (date_from, date_to))
                else:
                    if date_from:
                        clause.append("cot_updated_at>='%s'" % date_from)
                    if date_to:
                        clause.append("cot_updated_at<='%s'" % date_to)

        last_updated_filter = or_(*clause)

        if last_updated_filter is None:
            retailers_query = query.subquery('retailers_query')
        else:
	    temp = query.subquery('temp')
	    retailers_query = model.Session.query(
                temp.c.dome_identifier,
                temp.c.retailer_code).filter(last_updated_filter).subquery('retailers_query')

        query = model.Session.query(
          self.entity.dome_identifier,
          self.entity.retailer_code,
          model.Retailer.business_name,
          coalesce(model.RetailerPrincipal.retailer_category_raw, 'Kraft').\
            label('retailer_category_raw'),
          model.Retailer.status,
          self.entity.retailer_attribute_code,
          self.entity.range_value,
          self.entity.boolean_value,
          self.entity.text_value,
          model.Retailer.created_at,
          self.entity.cot_updated_at
        ).outerjoin(
            model.Retailer,
            and_(self.entity.dome_identifier == model.Retailer.dome_identifier,
                 self.entity.retailer_code == model.Retailer.code)
        ).outerjoin(
            model.RetailerPrincipal,
            and_(self.entity.dome_identifier == model.RetailerPrincipal.dome_identifier,
                 self.entity.retailer_code == model.RetailerPrincipal.retailer_code)
        ).join(
            model.RetailerAttribute,
            and_(self.entity.principal_code == model.RetailerAttribute.principal_code,
                 self.entity.retailer_attribute_code == model.RetailerAttribute.code)
        ).join(
            retailers_query,
            and_(model.Retailer.dome_identifier == retailers_query.c.dome_identifier,
                 model.Retailer.code == retailers_query.c.retailer_code))

        order_by_columns = ['dome_identifier', 'retailer_code',
                            'retailer_attribute_code']
	query = query.order_by(
		    *[getattr(self.entity, column) for column in\
		      order_by_columns])
	self.entity.write_header_row(csv_writer)
        for instance in query:
            if isinstance(instance, dict):
                csv_writer.writerow(instance)
            else:
                csv_dict = vars(instance)
                csv_writer.writerow(csv_dict)
        export.csv_file.flush()
	import zipfile
        zip_obj = zipfile.ZipFile(
            export.zip_file_pointer, 'w', zipfile.ZIP_DEFLATED)
        zip_obj.write(export.csv_file.name, export.archive_filename)
        zip_obj.close()
        export.zip_file_pointer.seek(0)
        content = export.zip_file_pointer.read()
        response.headers['Content-Type'] = "application/zip; charset=utf-8"
        response.headers['Content-Disposition'] = \
            "attachment; filename=%s" % export.zip_filename
        return content
