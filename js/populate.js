$(document).ready(function(){
  $('#principal').ready(onPrincipalChange);
  $("#principal").change(onPrincipalChange);
  $("#retailer_attribute_code").change(onRetailerAttributeCodeChange);
  $('form[action]').submit(onSubmitClick);
});

function onSubmitClick() {
  var input_type = $('#input_type').text();
  switch(input_type) {
    case 'Boolean':
      $('#min_value').val('');
      $('#max_value').val('');
      break;
    case 'Range':
      $('#bool_value').val('');
      break;
  }
  return true;
}

function onPrincipalChange()
{
  var principal_id = $("#principal").val();
  populate_retailer_category_options(principal_id);
  populate_retailer_attribute_code_options(principal_id);
}

function populate_retailer_category_options(principal_id){
  $.getJSON($('#base_url').attr("value") +
    "retailerAttributeCategories/populate_retailercategory_options?principal_id=" + principal_id,
    function(retailer_categories_json){
      var current_retailer_category_id = $('#current_retailer_category_id').val();
      $("#retailer_category").html(createSelectOptionsFromJSON(retailer_categories_json, ''));
      $('#retailer_category').val(current_retailer_category_id);
    });
}

function populate_retailer_attribute_code_options(principal_id)
{
  $.getJSON($('#base_url').attr("value") +
    "retailerAttributeCategories/populate_retailerattributecode_options?principal_id=" + principal_id,
    function(retailer_attribute_codes_json){
      var current_retailer_attribute_code = $('#current_retailer_attribute_code').val();
      $("#retailer_attribute_code").html(createSelectOptionsFromJSON(retailer_attribute_codes_json, ''));
      $('#retailer_attribute_code').val(current_retailer_attribute_code);
      onRetailerAttributeCodeChange();
    });
}

function onRetailerAttributeCodeChange() {
  $('#description').text('');
  $('#input_type').text('');
  var principal_id = $('#principal').val();
  var retailer_attribute_code = $('#retailer_attribute_code').val();
  if (retailer_attribute_code !== null) {
    $.getJSON($('#base_url').attr("value") +
      "retailerAttributeCategories/get_retailer_attribute?principal_id=" + principal_id + "&retailer_attribute_code=" + retailer_attribute_code,
      function(retailer_attribute_json) {
        $('#description').html(retailer_attribute_json.description);
        $('#input_type').html(retailer_attribute_json.input_type);
	onInputTypeChange();
      }
    );
  }
}

function onInputTypeChange() {
  var input_type = $('#input_type').text();
  switch(input_type) {
    case 'Boolean':
      $('#boolean').show();
      $('#range_min').hide();
      $('#range_max').hide();
      break;
    case 'Range':
      $('#range_min').show();
      $('#range_max').show();
      $('#boolean').hide();
      break;
    case '':
      $('#range_min').hide();
      $('#range_max').hide();
      $('#boolean').hide();
      break;
  }
}
